import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import locales from "./locales/locales";

i18next
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        load: 'languageOnly',
        resources: locales,
        fallbackLng: "en",
        interpolation: {
            escapeValue: false
        },
        react: {
            useSuspense: false
        }
    });