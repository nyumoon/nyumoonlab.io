import translationEN from './en/translation.json';
import translationDE from './de/translation.json';

function translation(t: any) {
    return {
        translation: t
    }
}

const locales = {
    en: translation(translationEN),
    de: translation(translationDE)
}

export default locales